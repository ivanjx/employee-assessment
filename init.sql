-- Database definition (MySQL)
DROP DATABASE IF EXISTS db_employee_assessment;
CREATE DATABASE db_employee_assessment;
USE db_employee_assessment;

-- users table.
CREATE TABLE users
(
    id INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(32) NOT NULL,
    password_hash VARCHAR(64) NOT NULL, -- We are using sha256
    password_pads VARCHAR(64) NOT NULL, -- Random string
    type VARCHAR(10) NOT NULL,
    employee_id VARCHAR(30) NOT NULL,
    full_name VARCHAR(50) NOT NULL,
    PRIMARY KEY(id),
    UNIQUE(username),
    UNIQUE(password_hash),
    UNIQUE(password_pads)
) ENGINE = InnoDB;

-- employee_groups table.
CREATE TABLE employee_groups
(
    leader_id INT NOT NULL,
    employee_id INT NOT NULL,
    FOREIGN KEY(leader_id) REFERENCES users(id) ON DELETE CASCADE,
    FOREIGN KEY(employee_id) REFERENCES users(id) ON DELETE CASCADE
) ENGINE = InnoDB;

-- common_tasks table.
CREATE TABLE common_tasks
(
    id INT NOT NULL AUTO_INCREMENT,
    task_name VARCHAR(100) NOT NULL,
    PRIMARY KEY(id)
) ENGINE = InnoDB;

-- employee_tasks table.
CREATE TABLE employee_tasks
(
    id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    task_title VARCHAR(100) NOT NULL,
    start_time DATETIME NOT NULL,
    end_time DATETIME NOT NULL,
    details TEXT,
    PRIMARY KEY(id),
    FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE
) ENGINE = InnoDB;

-- Creating mysql user.
DROP USER IF EXISTS employee_assessment;
CREATE USER employee_assessment IDENTIFIED BY '12345678';
GRANT INSERT, SELECT, DELETE, UPDATE ON db_employee_assessment.* TO employee_assessment;