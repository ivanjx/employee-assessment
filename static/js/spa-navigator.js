function spaNavigate(page, replace) {
    // Starting loading animation.
    $('#spinner').show();

    // Getting page via ajax.
    $.ajax({
        url: page + (page != '/' ? '/?ml=1' : ''),
        method: 'get',
        success: function(result) {
            // Load to content div.
            $('#content').html(result);

            // Pushing browser history.
            let state = {
                page: page
            };

            if(replace) {
                window.history.replaceState(state, 'Employee Assessment', page);
            } else {
                window.history.pushState(state, 'Employee Assessment', page);
            }

            // Stopping loading animation.
            $('#spinner').hide();
        },
        error: function(xhr, status, error) {
            //let err = JSON.parse(xhr.responseText);
            alert('Unable to connect to the server.\nDetails: ' + xhr.statusText);

            // Stopping loading animation.
            $('#spinner').hide();
        }
    });
}

function spaBack(page) {
    // Starting loading animation.
    $('#spinner').show();

    // Getting page via ajax.
    $.ajax({
        url: page + (page != '/' ? '/?ml=1' : ''),
        method: 'get',
        success: function(result) {
            // Load to content div.
            $('#content').html(result);
            
            // Stopping loading animation.
            $('#spinner').hide();
        },
        error: function(xhr, status, error) {
            //let err = JSON.parse(xhr.responseText);
            alert('Unable to connect to the server.\nDetails: ' + xhr.statusText);

            // Stopping loading animation.
            $('#spinner').hide();
        }
    });
}