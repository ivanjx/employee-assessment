// Copyright (C) 2018 Ivan Kara
// All server's configurations.

exports.server = {
    port: 3000,
    name: 'Employee Assessment System',
    version: '1.0',
    developer: 'Ivan Kara'
};

exports.dbConnPool = {
    connectionLimit: 100,
    host: 'localhost',
    user: 'employee_assessment',
    password: '12345678',
    database: 'db_employee_assessment'
};