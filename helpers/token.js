// All token related functions go here.
const Random = require('./random');
const User = require('./user');

// Getting user info by token.
// Callback = function(err, userInfo)
exports.getUserInfo = function(dbConn, redisClient, token, callback) {
    // Getting user id from redis.
    redisClient.get(token, function(err, reply) {
        if(err) {
            callback(err, null);
            return;
        }

        // Parsing.
        let redisUserInfo = JSON.parse(reply);
        
        // Using user helper.
        User.getUserInfoById(dbConn, redisUserInfo.id, function(err, userInfo) {
            if(err) {
                callback(err, null);
                return;
            }

            callback(null, userInfo);
        });
    });
};

// Checking user token on redis only.
// Callback = function(err, userInfo)
exports.check = function(redisClient, token, callback) {
    redisClient.get(token, function(err, reply) {
        if(err) {
            callback(err);
            return;
        }

        // Reading user id and type.
        let userInfo = JSON.parse(reply);
        callback(null, userInfo);
    });
};

// Create token and save it into the redis db.
// Callback = function(err, token)
exports.create = function(redisClient, userId, userType, callback) {
    // Generating token.
    let token = Random.randString(32);

    // Writing user id and type to redis.
    let value = JSON.stringify({ id: userId, type: userType })
    redisClient.set(token, value, function(err, reply) {
        if(err) {
            callback(err, null);
            return;
        }

        // Setting TTL for 30 minutes.
        redisClient.expire(token, 30 * 60);

        // Done.
        callback(null, token);
    });
};

// Delete token from redis db.
// Callback = function(err)
exports.delete = function(redisClient, token, callback) {
    // Deleting token from redis.
    redisClient.del(token);

    // Done.
    callback(null);
};

// Extend expirity time of token.
// Callback = function(err)
exports.extend = function(redisClient, token, callback) {
    // Checking token validity.
    exports.check(redisClient, token, function(err) {
        if(err) {
            callback(err);
            return;
        }

        // Extending TTL for the next 30 minutes.
        //let extendedTime = new Date();
        //extendedTime.setMinutes(extendedTime.getMinutes() + 30);
        //redisClient.expireat(token, parseInt(extendedTime / 1000));
        redisClient.expire(token, 30 * 60);

        // Done.
        callback(null);
    });
};