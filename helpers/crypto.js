// Cryptographic functions down here.
const crypto = require('crypto');

// SHA 256 hash generator from plain string.
exports.sha256Hash = function(input) {
    let hash = crypto.createHash('sha256');
    hash.update(input, 'utf8');
    return hash.digest('hex');
};