// Only for logging purpose.
// Not really important.
const dateFormat = require('dateformat');

// Print log message to stdout.
module.exports = function(message) {
    let dateStr = dateFormat(new Date(), 'HH:MM:ss');
    console.log('[' + dateStr + '] ' + message);
};