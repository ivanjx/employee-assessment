// Random string and number generation functions.
const possibleChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789#$%&*";

// Generating random number.
exports.randNumber = function(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
};

// Generating random string with help of randNumber.
exports.randString = function(length) {
    let text = "";
  
    for (let i = 0; i < length; i++) {
        text += possibleChars.charAt(exports.randNumber(0, possibleChars.length));
    }
  
    return text;
};
