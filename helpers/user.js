// All user related functions go here.
const Random = require('./random');
const Crypto = require('./crypto');

// Function to validate user login.
// Callback = function(err, userId, userType)
exports.validatePassword = function(dbConn, username, plainPassword, callback) {
    // Getting password pads.
    let query = 'SELECT password_pads FROM users WHERE username = ?';
    dbConn.query(query, [username], function(err, results) {
        if(err) {
            callback(err, null, null);
            return;
        }

        if(results.length == 0) {
            // No username found.
            callback(new Error('Username not found'), null, null);
            return;
        }

        let passwordPads = results[0].password_pads;

        // Generating password hash.
        // Password hash formula = plain password + password pads.
        let passwordHash = Crypto.sha256Hash(plainPassword + passwordPads);

        // Testing password hash value.
        let query = 'SELECT id, type FROM users WHERE password_pads = ?';
        dbConn.query(query, [passwordPads], function(err, results) {
            if(err) {
                callback(err, null, null);
                return;
            }

            // Checking results.
            if(results.length == 0) {
                // Invalid passwords (not found).
                callback(new Error('Invalid username and password combination.'), null, null);
                return;
            }

            // Success.
            callback(null, results[0].id, results[0].type);
        });
    });
};

// Create new user function.
// Callback = function(err)
exports.create = function(dbConn, userInfo, callback) {
    
};

// Get user info by user id.
// Callback = function(err, userInfo)
exports.getUserInfoById = function(dbConn, userId, callback) {

};

// Update user info.
// Callback = function(err)
exports.updateUserInfo = function(dbConn, userInfo, callback) {

};

// Delete user.
// Callback = function(err)
exports.delete = function(dbConn, userId, callback) {

};