// Copyright (C) 2018 Ivan Kara
// Entry point of the entire server app.

// Loading config.
const Config = require('./config');

// Loading mysql conn pool.
const mysql = require('mysql');
const connPool = mysql.createPool(Config.dbConnPool);
connPool.on('error', function(err) {
    console.log('DB Connection Error: ' + err.message);
});

// Setting up express.
const express = require('express');
const app = express();

// Adding body parser.
const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Adding cookie parser.
const cookieParser = require('cookie-parser');
app.use(cookieParser());

// Setting up render engine.
app.set('view engine', 'ejs');
const ejs = require('ejs');

// Defining static files.
app.use('/static', express.static('static'));

// Defining routes.
const renderWithMain = function(res, template, replaceState, pageSettings) {
    res.render(template, pageSettings, function(err, html) {
        if(err) {
            res.status(500);
            res.json({ success: false, message: err.message });
            return;
        }

        // Render the main page.
        res.render('pages/main', {
            server: Config.server,
            content: html,
            replaceState: replaceState
        });
    });
}

// View routes.
app.get('/', function(req, res) {
    // Checking cookie.
    let content;

    if(!req.cookies.token) {
        // Not logged in.
        // Rendering login page.
        renderWithMain(res, 'pages/login', '/login', null);
    }
});

app.get('/login', function(req, res) {
    if(!req.query.ml) {
        // Render with main page.
        renderWithMain(res, 'pages/login', '/login', null);
    } else {
        // Just render the content.
        res.render('pages/login');
    }
});

app.get('/admin', function(req, res) {
    // Rendering default inner page.
    res.render('pages/admin/hello', null, function(err, html) {
        if(err) {
            res.status(500);
            res.json({ success: false, message: err.message });
            return;
        }

        // Rendering admin panel page.
        if(!req.query.ml) {
            // Render with main page.
            renderWithMain(res, 'pages/admin/main', '/admin/hello', {
                content: html
            });
        } else {
            // Just render the content.
            res.render('pages/admin/main', {
                content: html
            });
        }
    });
});

app.get('/admin/hello', function(req, res) {
    // Render the hello page first.
    res.render('pages/admin/hello', null, function(err, html) {
        if(err) {
            res.status(500);
            res.json({ success: false, message: err.message });
            return;
        }

        // Rendering admin panel page.
        if(!req.query.ml) {
            // Render with main page.
            renderWithMain(res, 'pages/admin/main', '/admin/hello', {
                content: html
            });
        } else {
            // Just render the content.
            res.render('pages/admin/main', {
                content: html
            });
        }
    });
});

// API routes.


// Handle 404
app.use(function(req, res) {
    res.status(404);
    res.json({ success: false, message: 'Not found' });
});

// Handle 500
app.use(function(error, req, res, next) {
    res.status(500);
    res.json({ success: false, message: error.message });
});

// Starting server.
app.listen(Config.server.port, function(err) {
    if(err) {
        console.log('Unable to start http server: ' + err.message);
        return;
    }

    console.log('Server listening on port: ' + Config.server.port);
});