// Used for any types of authentication.
module.exports = function(connPool, redisClient) {
    const Token = require('../helpers/token');
    const User = require('../helpers/user');

    let module = {};

    // POST
    // This route will create token to redis.
    module.login = function(req, res) {
        // Checking cookie.
        if(req.cookies.token != null) {
            // Clearing token.
            res.clearCookie('token');
            res.json({ success: false, message: 'Token exists.' });
            return;
        }

        if(req.body.username == null || req.body.plainPassword == null) {
            // Insufficient parameter.
            res.json({ success: false, message: 'Insufficient parameter.' });
            return;
        }

        // Getting db conn.
        connPool.getConnection(function(err, dbConn) {
            if(err) {
                res.json({ success: false, message: err.message });
                return;
            }

            // Using helper.
            User.validatePassword(dbConn, req.body.username, req.body.plainPassword, function(err, userId, userType) {
                // Releasing mysql conn.
                dbConn.release();

                // Checking helper response.
                if(err) {
                    // Could be bad password or mysql related error.
                    res.json({ success: false, message: err.message });
                    return;
                }

                // Good password.
                // Creating token and save it into redis (using helper).
                Token.create(redisClient, userId, userType, function(err, token) {
                    if(err) {
                        // Redis related error.
                        res.json({ success: false, message: err.message });
                        return;
                    }

                    // Success.
                    // Setting cookie.
                    res.cookie('token', token, {
                        maxAge: 30 * 60 * 1000,
                        httpOnly: true, // Disable javascript access.
                        //secure: true // We dont have tls certificate yet.
                        sameSite: 'strict'
                    });

                    // Sending success response.
                    res.json({ success: true });
                });
            });
        });
    };

    // GET
    // This route will delete the token from redis.
    module.logout = function(req, res) {
        // Checking cookie.
        if(req.cookies.token == null) {
            // Token not available.
            res.json({ success: false, message: 'Token not available.'});
            return;
        }

        // Using helper.
        Token.delete(redisClient, req.cookies.token, function(err) {
            if(err) {
                res.json({ success: false, message: err.message });
                return;
            }

            // Clearing cookie.
            res.clearCookie(req.cookies.token);
            res.json({ success: true });
        });
    };

    // GET
    // This route will extend the expiration time of the token.
    module.extend = function(req, res) {
        // Checking cookie.
        if(req.cookies.token == null) {
            // Token not available.
            res.json({ success: false, message: 'Token not available' });
            return;
        }

        // Using helper.
        Token.extend(redisClient, req.cookies.token, function(err) {
            if(err) {
                res.json({ success: false, message: err.message });
                return;
            }

            // Extending token in cookie.
            res.cookie('token', req.cookies.token, {
                maxAge: 30 * 60 * 1000,
                httpOnly: true, // Disable javascript access.
                //secure: true // We dont have tls certificate yet.
                sameSite: 'strict'
            });

            // Sending response.
            res.json({ success: true });
        });
    };

    // GET
    // This route only checks whether the token is expired/not available or not.
    module.check = function(req, res) {
        // Checking cookie.
        if(req.cookies.token == null) {
            // Token not available.
            res.json({ success: false, message: 'Token not available.' });
            return;
        }

        // Using helper.
        Token.check(redisClient, req.cookies.token, function(err, userInfo) {
            if(err) {
                // Token not available or expired.
                res.json({ success: false, message: err.message });
                return;
            }

            res.json({ success: true, userInfo: userInfo });
        });
    };

    // GET
    // This route will return current user info by token.
    module.getUserInfo = function(req, res) {
        // Checking cookie.
        if(req.cookies.token == null) {
            // Token not available.
            res.json({ success: false, message: 'Token not available.' })
            return;
        }

        // Getting user id.
        redisClient.get(req.cookies.token, function(err, reply) {
            if(err) {
                res.json({ success: false, message: err.message });
                return;
            }

            let userId = parseInt(reply);

            // Getting db conn.
            connPool.getConnection(function(err, dbConn) {
                if(err) {
                    res.json({ success: false, message: err.message });
                    return;
                }

                // Using helper.
                User.getUserInfoById(dbConn, userId, function(err, userInfo) {
                    // Releasing db conn.
                    dbConn.release();

                    // Checking for error.
                    if(err) {
                        res.json({ success: false, message: err.message });
                        return;
                    }

                    // Done.
                    res.json({ success: true, userInfo: userInfo });
                });
            });
        });
    };

    return module;
}