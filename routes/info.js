// Used to retrieve server info.
module.exports = function(serverInfo) {
    let module = {};

    module.getInfo = function(req, res) {
        res.json(serverInfo);
    }

    return module;
}